import 'dart:math';

import 'package:flutter/material.dart';
import 'package:listbuilder_provider_sort/items_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ItemsProvider.instance,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'Flutter Demo Home Page'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ItemsProvider provider = ItemsProvider.instance;

  void sortItems() {
    provider.changeSortType();
    provider.sort();
  }

  void removeItem(int length) {
    Random random = Random();
    int randomInt = random.nextInt(length);
    provider.removeItem(randomInt);
  }

  void createList() {
    Random random = Random();
    int randomInt = random.nextInt(20);
    provider.createList(randomInt);
  }

  @override
  Widget build(BuildContext context) {
    var itemData = Provider.of<ItemsProvider>(context);
    final listItems = itemData.items;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 500,
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return Container(
                      height: 50,
                      child: Text(
                        '${listItems[index]}',
                        textAlign: TextAlign.center,
                      ),
                    );
                  },
                  itemCount: listItems.length,
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: Icon(Icons.format_indent_decrease),
                  onPressed: createList,
                ),
                IconButton(
                  icon: Icon(Icons.sort),
                  onPressed: sortItems,
                ),
                IconButton(
                  icon: Icon(Icons.delete_forever),
                  onPressed:() {removeItem(listItems.length);},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
