import 'package:flutter/material.dart';

class ItemsProvider with ChangeNotifier {
  ItemsProvider._privateConstructor();

  static final ItemsProvider _instance = ItemsProvider._privateConstructor();

  static ItemsProvider get instance => _instance;

  // int _listLength = 0;
  bool _isASCSort = true;

  List<int> _items = [];

  List<int> get items {
    return [..._items];
  }

  // int get listLength {
  //   return _listLength;
  // }

  void createList(int length) {
    _items = List.generate(length, (index) => index);
    notifyListeners();
  }

  void removeItem(int ind) {
    if (ind < _items.length) {
      _items.removeAt(ind);
      // _listLength--;
      notifyListeners();
    }
  }

  void changeSortType() {
    _isASCSort = !_isASCSort;
  }

  void sort() {
    if (_isASCSort) {
      _items.sort((a, b) => b.compareTo(a));
    } else {
      _items.sort((a, b) => a.compareTo(b));
    }
    notifyListeners();
  }
}
